import numpy

from math import sqrt, exp

from array import array

from scipy.signal import *

def normalize(input):
    output = []

    input = numpy.absolute(input)

    min_val = min(input)
    max_val = max(input)

    for val in input:
        val = (val - min_val) / (max_val - min_val)

        output.append(val)

    return output

def reduce_low(input):
    output = []

    top_threshold = numpy.quantile(input, q = 0.9)
    zero_threshold = numpy.quantile(input, q = 0.1)

    for val in input: 
        if val <= zero_threshold:
            val = 0.0
        elif val <= top_threshold:
            val = sqrt(val)

        output.append(val)

    return output 

def savgol(input):
    return savgol_filter(input, window_length=2**12 + 1, polyorder=4)

def median(input):
    return medfilt(input, kernel_size=2**8+1)