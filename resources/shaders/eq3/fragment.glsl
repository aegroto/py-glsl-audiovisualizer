/*************/
/* EQ SHADER */
/*************/

#version 330

uniform float time;

uniform float max_con;
uniform float prev_max_con;
uniform float slice_prog;
uniform float intensity;

uniform vec2 window_size;

uniform sampler2D background_texture;
uniform sampler2D eqmap_texture;
uniform sampler2D freqmap_texture;
uniform sampler2D prev_freqmap_texture;

in vec2 uv0;

out vec4 fragColor;

float rand(vec2 co){
    return fract(sin(dot(co.xy, vec2(12.9898,78.233))) * 43758.5453);
}

float sign(float val) {
    return abs(val) / val;
} 

void main() {
    float time_step = time * 0.2;
    time_step = min(fract(time_step), 1.0 - fract(time_step));

    vec2 uv = uv0;

    // Zoom changes with intensity
    float bg_zoom_factor = .3 * intensity;

    // Calculate background color
    vec2 bg_uv = (uv + uv * bg_zoom_factor) - (bg_zoom_factor * .5);

    bg_uv = clamp(bg_uv, 0.0, 1.0);

    float noise_appearance_factor = max(0.0, (rand(vec2(time_step + uv.x, time_step + uv.y)) - .95) * 10.0);
    float intensity_noise_appearance_factor = (rand(vec2(time_step + uv.y, time_step + uv.x)) > .95 ? 1.0 : 0.0);

    vec4 bg_color_noise = vec4(
        rand(vec2(time * .4, time * .5)),
        rand(vec2(time_step, time * .1)),
        rand(vec2(time * .3, time_step)),
        0.0
    ) * max(noise_appearance_factor, (0.2 + intensity) * intensity_noise_appearance_factor);

    vec4 bg_color = texture(background_texture, bg_uv) + bg_color_noise;

    float bg_brightness_factor = 0.65 + 0.35 * time_step;
    bg_color *= bg_brightness_factor * mix(0.8, 1.0, rand(bg_uv.yx));

    // Quantize the horizontal coordinate to create 'bars'
    // float quantization_factor = 0.02;
    // float space_between_bars = 0.001;

    // float bar_coeff = abs(uv.x - (floor(uv.x / quantization_factor) * quantization_factor)) < space_between_bars ? 0.0 : 1.0;

    // Diffuse color

    // Scale the coordinates by the quantization factor
    // float bar_size = quantization_factor * 100.0;
    // uv.x = (floor((uv.x * 100.0) / bar_size) * bar_size) / 100.0;

    // Get con factor
    float frequency = texture(freqmap_texture, uv).r;
    float prev_frequency = texture(prev_freqmap_texture, uv).r;

    float con = frequency / max_con;
    float prev_con = prev_frequency / prev_max_con;

    float eq_move_factor = (prev_con * (1.0 - slice_prog) + con * (slice_prog)) * max(.25, intensity);
    float weighted_con = .6 + eq_move_factor * .05;
    // weighted_con = weighted_con * 0.2 + weighted_con * intensity * 0.8;
    // weighted_con *= 5.0;

    //  Calculate the mask's value
    float mask_value = weighted_con > uv.y ? smoothstep(0.98, 1.0, weighted_con * 0.9 / uv.y) : 0.0;
    // float mask_value = smoothstep(0.0, 5.0, weighted_con - uv.y);
    // mask_value = mask_value > 0.2 ? mask_value : 0.0;
    // mask_value *= bar_coeff;

    float eq_translate_factor = .025 * intensity * (fract(time) - .5);
    vec2 eq_uv = (uv + eq_translate_factor);
    eq_uv = clamp(eq_uv, 0.0, 1.0);

    vec4 eq_map_color = texture(eqmap_texture, eq_uv);
    eq_map_color = vec4(
        eq_map_color.r * mix(con, 1.0, 0.95),
        eq_map_color.g * mix(weighted_con, 1.0, 0.95),
        eq_map_color.b * mix(prev_con, 1.0, 0.95),
        eq_map_color.a
    );

    vec4 eq_map_gray = vec4(
        vec3((eq_map_color.r + eq_map_color.g + eq_map_color.b) / 3.0), 
        min(0.3, eq_map_color.a)
    );

    vec4 eq_color = mix(eq_map_gray, eq_map_color, mask_value);

    fragColor = mix(bg_color, eq_color, eq_color.a);
}