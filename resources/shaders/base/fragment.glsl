#version 330

uniform float time;
uniform float start_time;
uniform float speed;
uniform float current_intensity;
uniform float current_frequency;

uniform vec2 window_size;

uniform sampler2D image_texture;
uniform sampler2D distmap_texture;

in vec2 uv0;

out vec4 fragColor;

float rand(vec2 co){
    return fract(sin(dot(co.xy, vec2(12.9898,78.233))) * 43758.5453);
}

float sign(float val) {
    return abs(val) / val;
} 

float grayscale(vec3 color) {
    return (color.r + color.g + color.b) / 3.0;
}

float yellowscale(vec3 color) {
    return (color.r + color.g) / 2.0;
}

vec2 scale_uv(vec2 uv, float scale_factor) {
    return (uv * vec2(1.0 - scale_factor)) + scale_factor/2.0;
}

void main() {
    float time_var = (time - start_time) * speed;

    float tick_factor = fract(time_var);
    tick_factor = tick_factor > 0.5 ? 1.0 - tick_factor : tick_factor;

    // Calculate image scaling factor
    float intensity_scale_factor = current_intensity * 0.0001;
    float frequency_scale_factor = current_frequency * 0.001;

    // Dist UV and color
    vec2 dist_uv = uv0;

    dist_uv.x = dist_uv.x * 1.0;
    dist_uv.y = dist_uv.y + frequency_scale_factor * 1.0;

    dist_uv = scale_uv(dist_uv, frequency_scale_factor * 0.00001);
    dist_uv *= 1.0;

    vec4 dist_color = texture(distmap_texture, dist_uv);

    // Image UV and color
    vec2 uv = uv0;

    vec2 translation = vec2(
        (rand(uv.xy) * 0.0075 * rand(vec2(tick_factor)) + (intensity_scale_factor * 0.01)) * sign(fract(time_var * 1.0) - 0.5),
        rand(uv.yx) * 0.0025 * rand(vec2(-tick_factor)) // + current_intensity * 0.05
    );

    uv.x = uv.x + translation.x;
    uv.y = uv.y + translation.y;

    // uv = uv * vec2(1.0 - intensity_scale_factor) + intensity_scale_factor/2.0;
    float base_scale = 0.2;

    uv = scale_uv(uv, clamp(base_scale + intensity_scale_factor * 0.1, 0.1, 0.5));

    vec4 image_color = texture(image_texture, uv); 

    vec3 image_brightness = vec3(clamp(-0.1 + frequency_scale_factor, -1.0, 0.3)); // (1.0 - tick_factor * 0.5);

    float background_beat_force = frequency_scale_factor * 1.5;
    
    vec3 bbf_contributions = vec3(0.0);
    bbf_contributions.r = background_beat_force * floor(rand(vec2(time_var)) + 0.5);
    bbf_contributions.g = background_beat_force - bbf_contributions.r;
    bbf_contributions.b = 0.0;

    vec3 noise_color = vec3(
        0.2 + rand(vec2(frequency_scale_factor)) * 0.03,
        0.3 + rand(vec2(intensity_scale_factor)) * 0.03,
        0.5 + tick_factor * 0.4
    );

    float bbf_factor = 0.075;

    noise_color = noise_color * (1.0 - bbf_factor) + bbf_contributions * bbf_factor;

    // Final color mashup
    float noise_fade = rand(vec2(dist_uv.xx)) - 0.1;

    vec4 out_color = 
        mix(
            image_color + vec4(image_brightness, 0.0),
            vec4(noise_color, 1.0) * dist_color * 0.5 - noise_fade,
            // 1.0 - yellowscale(image_color.rgb) * 0.8 - noise_fade 
            1.0 - yellowscale(image_color.rgb)
        );

    fragColor = out_color;
}