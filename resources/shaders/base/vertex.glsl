#version 330

in vec3 in_position;
in vec2 in_texcoord_0;

out vec2 uv0;

void main() {
    vec4 position = vec4(in_position, 1);

    position.x *= 1.0;
    position.y *= 1.0;

    gl_Position = position;

    uv0 = in_texcoord_0;
}

