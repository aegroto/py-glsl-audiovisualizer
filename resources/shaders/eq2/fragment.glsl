/*************/
/* EQ SHADER */
/*************/

#version 330

uniform float time;

uniform float max_con;
uniform float prev_max_con;
uniform float slice_prog;
uniform float intensity;

uniform vec2 window_size;

uniform sampler2D background_texture;
uniform sampler2D eqmap_texture;
uniform sampler2D freqmap_texture;
uniform sampler2D prev_freqmap_texture;

in vec2 uv0;

out vec4 fragColor;

float rand(vec2 co){
    return fract(sin(dot(co.xy, vec2(12.9898,78.233))) * 43758.5453);
}

float sign(float val) {
    return abs(val) / val;
} 

void main() {
    float time_step = time * 0.2;
    time_step = min(fract(time_step), 1.0 - fract(time_step));

    vec2 uv = uv0;

    //Calculate background color
    vec2 bg_uv = uv;

    vec4 bg_color = texture(background_texture, bg_uv);

    float bg_brightness_factor = max(time_step, 0.8);
    bg_color *= bg_brightness_factor; // * mix(0.0, 1.5, rand(bg_uv.yx));

    // Quantize the horizontal coordinate to create 'bars'
    float quantization_factor = 0.002;
    float space_between_bars = 0.0;

    float bar_coeff = abs(uv.x - (floor(uv.x / quantization_factor) * quantization_factor)) < space_between_bars ? 0.0 : 1.0;

    // Diffuse color
    vec4 eq_color = texture(eqmap_texture, uv);

    // Scale the coordinates by the quantization factor
    float bar_size = quantization_factor * 100.0;
    uv.x = (floor((uv.x * 100.0) / bar_size) * bar_size) / 100.0;

    // Get con factor
    float con = texture(freqmap_texture, uv).r / max_con;
    float prev_con = texture(prev_freqmap_texture, uv).r / prev_max_con;

    float weighted_con = prev_con * (1.0 - slice_prog) + con * (slice_prog);
    weighted_con = weighted_con * 0.2 + weighted_con * intensity * 0.8;
    // weighted_con *= 5.0;

    //  Calculate the mask's value
    float mask_value = smoothstep(0.0, 3.0, weighted_con * 0.3 / abs(uv.x - 0.5));
    mask_value *= bar_coeff;

    vec4 out_color = eq_color * mask_value + bg_color * (1.0 - mask_value);

    float brightness = 0.5 + (intensity * 0.5);

    fragColor = out_color * brightness;
}