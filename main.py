#### HOTFIX ####
import os, logging
os.environ.setdefault('PATH', '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games')
#### HOTFIX ####

import moderngl 
import moderngl_window as mglw
import numpy
import yaml
import scipy
import traceback
import math
import os

from threading import Thread
from time import sleep

from moderngl_window import geometry, resources, screenshot
from moderngl_window.conf import settings

# from scipy.io import wavfile as wav
# from scipy.fftpack import fft
# from sklearn.preprocessing import normalize, minmax_scale

from pydub import AudioSegment
from pydub.playback import play

from pathlib import Path

from utils import *

from PIL import Image

class VideoWindow(mglw.WindowConfig):
    gl_version = (3, 3)
    window_size = (1280, 720)
    vsync = True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        shader_name = "eq3"
        song_name = "suspense"

        # Do initialization here
        self.prog = self.ctx.program(
            vertex_shader = open("resources/shaders/{}/vertex.glsl".format(shader_name), "r").read(),
            fragment_shader = open("resources/shaders/{}/fragment.glsl".format(shader_name), "r").read()
        )

        self.bg_texture = self.load_texture_2d("songs/{}/bg.png".format(song_name))
        self.eqmap = self.load_texture_2d("songs/{}/eqmap.png".format(song_name))

        self.quad = geometry.quad_fs()

        self.start_time = None
        self.speed = 0.3 

        self.current_frame = -1

        # Load audio
        self.audio = AudioSegment.from_file("resources/songs/{}/audio.wav".format(song_name), format="wav")
        self.samplerate = self.audio.frame_rate
        self.nyquist_frequence = self.samplerate / 2.0

        self.playing = False

        # Load samples
        try:
            self.samples = numpy.load("samples_cache.npy", allow_pickle=True)
        except:
            logging.info("Unable to load samples cache, generating from audio file...")

            self.samples = self.audio.get_array_of_samples()

            logging.info("Normalizing...")
            self.samples = numpy.abs(self.samples)

            logging.info("Reducing low intensities...")
            self.samples = reduce_low(self.samples)

            logging.info("Applying savgol filter...")
            self.samples = savgol(self.samples)

            logging.info("Applying median filter...")
            self.samples = median(self.samples)

            numpy.save("samples_cache.npy", self.samples)

        self.max_intensity = numpy.max(self.samples)

        # Load frequencies
        self.slice_size = int(4096) # int(self.samplerate / 12.0)
        self.n_slices = math.floor(len(self.samples) / self.slice_size)

        self.frequency_bins = numpy.fft.fftfreq(self.slice_size, 1.0/self.samplerate)
        self.frequency_bins = self.frequency_bins[:int(len(self.frequency_bins)/2)]

        # If frequencies are not cached, extract them
        if True: # not os.path.exists("frequencies_cache.npy"):
            logging.info("Unable to load frequencies cache, generating from samples...")

            self.frequencies = numpy.ndarray(shape= (self.n_slices, int(len(self.frequency_bins))))

            for i in range (0, self.n_slices):
                slice_data = self.samples[i * self.slice_size: (i + 1) * self.slice_size]
                # hamming_slice_data = slice_data * numpy.hamming((len(slice_data)))
                hamming_slice_data = slice_data

                fft = numpy.fft.fft(hamming_slice_data)
                fft = numpy.abs(hamming_slice_data)

                fft_real = fft[:int(len(fft)/2)]
                fft_imaginary = fft[int(len(fft)/2):]

                fft_module = fft_real ** 2 + fft_imaginary ** 2

                # frequency_bins = numpy.fft.fftfreq(len(fft), 1.0 / self.samplerate)
                # frequency_bins = frequency_bins[:int(len(frequency_bins)/2)]

                # logging.info(frequency_bins)
                # logging.info(fft)

                for b in range (0, len(self.frequency_bins)):
                    self.frequencies[i][b] = fft_module[b]

            numpy.save("frequencies_cache.npy", self.frequencies)

        self.frequencies = numpy.load("frequencies_cache.npy")

        # Create frequency textures
        logging.info("Generating frequency textures...")

        self.frequency_textures = [] 

        # self.max_frequency = numpy.max(self.frequency_bins)

        for t in range(0, len(self.frequencies)):
            slice_frequency_data = self.frequencies[t].astype(numpy.single)

            # Update maximum frequency
            # max_freq = numpy.max(slice_frequency_data)

            # if max_freq > self.max_frequency:
            #     self.max_frequency = max_freq
            
            # Divide frequencies for the maximum frequency
            slice_frequency_data # /= self.max_frequency

            texture = self.ctx.texture(
                size = (len(slice_frequency_data), 1),
                components = 1,
                data = slice_frequency_data,
                dtype = 'f4'
            )

            self.frequency_textures.append(texture)

        logging.info("Setup finished")

        sleep(2)

    def play_audio(self):
        play(self.audio)

    def set_uniform(self, key, value):
        try:
            self.prog[key] = value
        except KeyError:
            logging.debug(f"Unable to set uniform '{key}'")

    def set_uniform_value(self, key, value):
        try:
            self.prog[key].value = value
        except KeyError:
            logging.debug(f"Unable to set value of uniform '{key}'")

    def render(self, time, frametime):
        self.ctx.clear()

        self.current_frame += 1

        if not self.start_time:
            self.start_time = time
        
        if not self.playing:
            Thread(target = self.play_audio).start()
            self.playing = True
        
        # Calculate current audio sample
        sample_index = int(time * self.samplerate)
        if sample_index >= len(self.samples):
            logging.info("Simulation finished (no more samples)")
            exit(0)

        current_audio_sample = self.samples[sample_index]

        intensity = current_audio_sample / self.max_intensity

        # Recover frequencies
        current_slice = int(sample_index / self.slice_size)
        prev_slice = max(current_slice - 1, 0)

        if current_slice >= len(self.frequency_textures):
            logging.info("Simulation finished (no more frequency textures)")
            exit(0)

        frequency_map = self.frequency_textures[current_slice]
        previous_frequency_map = self.frequency_textures[prev_slice]

        slice_prog = (sample_index - (current_slice * self.slice_size)) / self.slice_size

        max_con = numpy.max(self.frequencies[current_slice])
        prev_max_con = numpy.max(self.frequencies[prev_slice])

        # logging.info("Current slice: {} ({})".format(current_slice, sample_index))

        # Load textures
        self.bg_texture.use(location = 0)
        self.eqmap.use(location = 1)
        frequency_map.use(location = 2)
        previous_frequency_map.use(location = 3)

        # Uniforms
        self.set_uniform('time', time)
        self.set_uniform_value('max_con', max_con)
        self.set_uniform_value('prev_max_con', prev_max_con)
        self.set_uniform_value('slice_prog', slice_prog)

        self.set_uniform('intensity', intensity)

        # self.prog['window_size'] = self.window_size 

        self.set_uniform_value('background_texture', 0)
        self.set_uniform_value('eqmap_texture', 1)
        self.set_uniform_value('freqmap_texture', 2)
        self.set_uniform_value('prev_freqmap_texture', 3)

        # self.prog['time'].value = time
        # self.prog['current_intensity'].value = current_audio_sample

        # self.prog['frequency_bins'].value = frequency_bins
        # self.prog['frequencies'].value = frequencies

        self.quad.render(self.prog)

        # screenshot.create(self.ctx.fbo, name = "frame{:05d}.png".format(self.current_frame))

def main():
    settings.SCREENSHOT_PATH = "output/"

    resources.register_dir((Path(__file__).parent / 'resources').resolve())

    mglw.run_window_config(VideoWindow)

if __name__ == "__main__":
    main()